module.exports = function (filestackHelpers, $, window, document, undefined) {
    "use strict";

    const pluginName = "fileViewerPlugin",
        defaults = {
            viewer: {
                handle: null,
                policy: null,
                signature: null,
            },
            client: {
                apikey: null,
                policySettings: {
                    policy: null,
                    signature: null,
                },
                cname: null,
            }
        };

    function Plugin(element, options) {
        this.element = element;
        this.containerElement = null;
        this.settings = $.extend(true, {}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    $.extend(Plugin.prototype, {
        init: function () {
            let $el = this._getElement(),
                id = $el.attr('id');

            if (id === undefined) {
                id = this._assignUniqueIdToElement();
            }

            this._initViewer();
            $(document).trigger('fileViewerPlugin.ready');
        },
        _assignUniqueIdToElement: function() {
            let baseid = 'filestackpreview',
                newid = baseid,
                unique = 0;

            while($('#' + newid).length) {
                ++unique;
                newid = baseid + unique.toString();
            }

            this._getElement().attr('id', newid);
            return newid;
        },
        _initViewer: function() {
            const client = filestackHelpers.getClient(this.settings.client.apikey, this.settings.client.policySettings,this.settings.client.cname);

            client.preview(this.settings.viewer.handle, {
                id: this._getElement().attr('id'),
                policy: this.settings.viewer.policy,
                signature: this.settings.viewer.signature,
            });
        },
        _getElement: function() {
            return $(this.element);
        }
    });

    $.fn[pluginName] = function (options) {
        const argsForCall = Array.prototype.slice.call(arguments, 1);
        this.each(function () {
            let existing, pluginDataKey = "plugin_" + pluginName;
            // if options is a string, assume we're trying to passively call a method on an existing instance and pass any further args to the method
            if (typeof options === 'string') {
                existing = $.data(this, pluginDataKey);
                if (existing && {}.toString.call(existing[options]) === '[object Function]') {
                    existing[options].apply(existing, argsForCall);
                }
            } else if (!$.data(this, pluginDataKey)) {
                $.data(this, pluginDataKey, new Plugin(this, options));
            }
        });

        return this;
    };
};
