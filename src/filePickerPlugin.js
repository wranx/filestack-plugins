module.exports = function (filestackHelpers, $, window, document, undefined) {
    "use strict";

    const pluginName = "filePickerPlugin",
        defaults = {
            text: {
                upload_file: '',
                change_file: '',
                view_file: '',
                html: {
                    upload_file: '',
                    change_file: '',
                    view_file: '',
                }
            },
            picker: {
                fromSources: ['local_file_system'],
                accept: ['.doc', '.docx', '.xls', '.xlsx', '.ppt', '.pptx', '.bmp', '.gif', '.tiff', '.ai', '.psd', '.png', '.jpeg', '.jpg', '.pdf', '.mp3'],
            },
            client: {
                apikey: null,
                policySettings: {
                    policy: null,
                    signature: null,
                },
                cname: null,
            },
            file_access_url: ''
        };

    function Plugin(element, options) {
        this.element = element;
        this.containerElement = null;
        this.settings = $.extend(true, {}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    $.extend(Plugin.prototype, {
        reinitialise: function (settings) {
            let initial_value;

            this.settings = $.extend(true, {}, defaults, settings);

            initial_value = this.settings.initial_value || this._getInput().val();

            this._setElementText(this._getLink(), 'view_file');
            this._setUrl(initial_value);
        },
        init: function () {
            const initial_value = this.settings.initial_value || this._getInput().val();
            this._initDOM();
            this._setUrl(initial_value);
            this._attachEvents();
            this._initFilePicker();
            $(document).trigger('filePickerPlugin.ready');
        },
        _initFilePicker: function initFilePicker() {
            this._getButton().click(function () {
                const client = filestackHelpers.getClient(this.settings.client.apikey,this.settings.client.policySettings,this.settings.client.cname);
                client.pick(this.settings.picker).then(this._handleFileSelection.bind(this));
                return false;
            }.bind(this));
        },
        _setUrl: function (e_url) {
            const url = (e_url && e_url.target)? e_url.target.value : e_url || '',
                currentUrl = this._getInput().val(url);

            if (currentUrl !== url) {
                this._getInput().val(url);
            }

            this._updateView(url != '');
        },
        _getUrl: function () {
            return this._getInput().val();
        },
        _attachEvents: function () {
            this._getInput().change(this._setUrl.bind(this));
        },
        _updateView: function (hasFileSet) {
            this._getLink().attr('href', this.settings.file_access_url + encodeURI(this._getUrl()));

            if (hasFileSet) {
                this._getLink().show();
                this._setElementText(this._getButton(), 'change_file');
                return;
            }

            this._getLink().hide();
            this._setElementText(this._getButton(), 'upload_file');
        },
        _initDOM: function () {
            const $outer = $('<div>').addClass('file-picker'),
                $link = $('<a href="#" target="_blank">'),
                $button = $('<button type="button" class="btn btn-default">'),
                $input = this._getInput();

            this._setElementText($link, 'view_file');
            this._setElementText($button, 'upload_file');

            this.containerElement = $outer.get(0);

            $outer.insertBefore($input);
            $outer.append($link);
            $outer.append($button);
            $outer.append($input);
            $(document).trigger('filePickerPlugin.DOMReady');
        },
        _handleFileSelection: function (response) {
            if (response['filesUploaded'] !== undefined && response['filesUploaded'][0]['url'] !== undefined) {
                this._getInput().val(response['filesUploaded'][0]['url']).trigger("change")
                    .trigger('filePickerPlugin.fileUploadFinished', response);
            }
            return false;
        },
        _getLink: function () {
            return this._getContainer().find('a');
        },
        _getInput: function () {
            return $(this.element);
        },
        _getContainer: function () {
            return $(this.containerElement);
        },
        _getButton: function () {
            return this._getContainer().find('button');
        },
        /**
         * The key for the text in this.settings.text[key], will automatically use html if the key is set in this.settings.text.html[key]
         * Will prefer the html option when available.
         * @param text_key
         * @private
         */
        _setElementText: function ($el, text_key) {
            let text = this.settings.text[text_key]||'';
            let html = this.settings.text.html[text_key]||'';
            if (html !== '') {
                $el.html(html);
                return;
            }
            $el.text(text);
        }
    });

    $.fn[pluginName] = function (options) {
        const argsForCall = Array.prototype.slice.call(arguments, 1);
        this.each(function () {
            let existing, pluginDataKey = "plugin_" + pluginName;
            // if options is a string, assume we're trying to passively call a method on an existing instance and pass any further args to the method
            if (typeof options === 'string') {
                existing = $.data(this, pluginDataKey);
                if (existing && {}.toString.call(existing[options]) === '[object Function]') {
                    existing[options].apply(existing, argsForCall);
                }
            } else if (!$.data(this, pluginDataKey)) {
                $.data(this, pluginDataKey, new Plugin(this, options));
            }
        });

        return this;
    };
};
