module.exports = function (filestackHelpers, $, window, document, undefined) {
    "use strict";

    const pluginName = "imagePickerPlugin",
        onSelectionEnum = {
            selectOnImageClick: 'select', // Hides the button and shows the image, clicking the image allows selection of a new image
            clearOnImageClick: 'clear', // Shows the upload button and the image, clicking the image clears the selection
            showButton: 'show' // Shows the upload button and the image, clicking the image does nothing
        },
        defaults = {
            text: {
                upload_thumbnail: '',
                change_thumbnail: '',
                clear_thumbnail: '',
            },
            picker: {
                fromSources: ['local_file_system', 'imagesearch'],
                accept: ['image/*'],
                maxSize: 1024 * 1024 * 10, // 10MB,
                storeTo: {
                    location: 's3'
                },
                transformations: {
                    crop: true,
                    rotate: true,
                    circle: true
                },
                uploadInBackground: false // See https://github.com/filestack/filestack-js/issues/117
            },
            client: {
                apikey: null,
                policySettings: {
                    policy: null,
                    signature: null,
                },
                cname: null,
            },
            file_access_url: '',
            // This was the old behaviour so best to have this as the default for now
            onSelection: onSelectionEnum.selectOnImageClick,
        };

    function Plugin(element, options) {
        this.element = element;
        this.containerElement = null;
        this.settings = $.extend(true, {}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    $.extend(Plugin.prototype, {
        reinitialise: function (settings) {
            let initial_value;

            this.settings = $.extend(true, {}, defaults, settings);

            initial_value = this.settings.initial_value || this._getInput().val();

            this._initOverlayText(this._getImageOverlay());
            this._getButton().text(this.settings.text.upload_thumbnail);

            this._setUrl(initial_value);
        },
        init: function () {
            const initial_value = this.settings.initial_value || this._getInput().val();
            let imgSelectors;
            this._initDOM();
            this._setUrl(initial_value);
            this._attachEvents();
            this._initImagePicker();
            $(document).trigger('imagePickerPlugin.ready');
        },
        _initImagePicker: function () {
            const client = filestackHelpers.getClient(this.settings.client.apikey, this.settings.client.policySettings, this.settings.client.cname);
            let $clickableUpdates = $([this._getButton()]).map( function () {return this.toArray(); } );
            let $clickableClears = $([]);
            let $faders = $([]);

            if (this.settings.onSelection === onSelectionEnum.clearOnImageClick) {
                $faders = $([this._getImage(), this._getImageOverlay()]).map(function () {
                    return this.toArray();
                });
                $clickableClears = $([this._getImage(), this._getImageOverlay()]).map( function () {return this.toArray(); } );
            }

            if (this.settings.onSelection === onSelectionEnum.selectOnImageClick) {
                $faders = $([this._getImage(), this._getImageOverlay()]).map(function () {
                    return this.toArray();
                });
                $clickableUpdates = $([this._getImage(), this._getImageOverlay(), this._getButton()]).map( function () {return this.toArray(); } );
            }

            $faders.on('mouseover', function () {
                let tenpct = 0.1 * this._getImage().width(), eightypct = 0.8 * this._getImage().width();
                let originalLeft = 0.5 * (this._getImage().parent().width() - this._getImage().width());
                this._getImage().css("opacity", "0.1").css("filter", "grayscale(100%)");
                if (this.settings.onSelection === onSelectionEnum.selectOnImageClick) {
                    this._getImageOverlay().show().css("left", "" + (originalLeft + tenpct) + "px").css("width", "" + eightypct + "px").css('text-align', 'center');
                } else {
                    this._getImageOverlay().show().css("left", "" + (originalLeft + tenpct) + "px").css("width", "" + eightypct + "px").css('text-align', 'right');
                }
                $('html,body').css('cursor', 'pointer');
            }.bind(this)).on('mouseout', function () {
                this._getImage().css("opacity", "1").css("filter", "");
                this._getImageOverlay().hide();
                $('html,body').css('cursor', 'default');
            }.bind(this));

            $clickableUpdates.on('click', function () {
                client.pick(this.settings.picker).then(this._handleFileSelection.bind(this));
                return false;
            }.bind(this));
            $clickableClears.on('click', function () {
                this._setUrl('');
                return false;
            }.bind(this));
        },
        _setUrl: function (e_url) {
            const url = (e_url && e_url.target)? e_url.target.value : e_url || '';

            this._getInput().val(url);
            this._updateView(url != '');
        },
        _getUrl: function () {
            return this._getInput().val();
        },
        _attachEvents: function () {
            this._getInput().change(this._setUrl.bind(this));
        },
        _updateView: function (hasImageSet) {
            if (this._getUrl() != '') {
                this._getImage().attr('src', this.settings.file_access_url + encodeURI(this._getUrl()));
            } else {
                this._getImage().attr('src', '');
            }

            if (hasImageSet) {
                this._getImageWrapper().show();
                if (this.settings.onSelection === onSelectionEnum.selectOnImageClick) {
                    this._getButton().hide();
                }
                return;
            }

            this._getImageWrapper().hide();
            this._getButton().show();
        },
        _initOverlayText: function ($imgoverlay) {
            if (this.settings.onSelection === onSelectionEnum.selectOnImageClick) {
                $imgoverlay.text(this.settings.text.change_thumbnail);
            } else if (this.settings.onSelection === onSelectionEnum.clearOnImageClick) {
                $imgoverlay.text(this.settings.text.clear_thumbnail);
            }
        },
        _initDOM: function () {
            const $outer = $('<div>').addClass('image-picker'),
                $imgwrap = $('<div>').addClass('image-wrap'),
                $img = $('<img src="">'),
                $imgoverlay = $('<span>').addClass('overlay'),
                $button = $('<button type="button" class="btn btn-default">').text(this.settings.text.upload_thumbnail),
                $input = this._getInput();

            this._initOverlayText($imgoverlay);

            this.containerElement = $outer.get(0);

            $imgwrap.append($img);
            $imgwrap.append($imgoverlay);

            $outer.insertBefore($input);
            $outer.append($imgwrap);
            $outer.append($button);
            $outer.append($input);
            $(document).trigger('imagePickerPlugin.DOMReady');
        },
        _handleFileSelection: function (response) {
            if (response['filesUploaded'] !== undefined && response['filesUploaded'][0]['url'] !== undefined) {
                this._getInput().val(response['filesUploaded'][0]['url']).trigger("change")
                    .trigger('imagePickerPlugin.imageUploadFinished', response);
            }
            return false;
        },
        _getInput: function () {
            return $(this.element);
        },
        _getContainer: function () {
            return $(this.containerElement);
        },
        _getImageWrapper: function () {
            return this._getContainer().find('.image-wrap');
        },
        _getImage: function () {
            return this._getContainer().find('img');
        },
        _getImageOverlay: function () {
            return this._getContainer().find('.overlay');
        },
        _getButton: function () {
            return this._getContainer().find('button');
        }
    });

    const plugin = $.extend(function (options) {
        const argsForCall = Array.prototype.slice.call(arguments, 1);
        this.each(function () {
            let existing, pluginDataKey = "plugin_" + pluginName;
            // if options is a string, assume we're trying to passively call a method on an existing instance and pass any further args to the method
            if (typeof options === 'string') {
                existing = $.data(this, pluginDataKey);
                if (existing && {}.toString.call(existing[options]) === '[object Function]') {
                    existing[options].apply(existing, argsForCall);
                }
            } else if (!$.data(this, pluginDataKey)) {
                $.data(this, pluginDataKey, new Plugin(this, options));
            }
        });

        return this;
    }, {onSelectionEnum: onSelectionEnum, defaults: defaults});

    $.fn[pluginName] = plugin;
};
