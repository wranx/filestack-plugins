module.exports = function (filestackHelpers, $, window, document, undefined) {
    "use strict";

    const pluginName = "multiFilePickerPlugin",
        defaults = {
            text: {
                upload_files: '',
                files_uploaded: '',
            },
            picker: {
                fromSources: ['local_file_system'],
                accept: ['.doc', '.docx', '.xls', '.xlsx', '.ppt', '.pptx', '.bmp', '.gif', '.tiff', '.ai', '.psd', '.png', '.jpeg', '.jpg', '.pdf', '.mp3'],
                maxFiles: 1000,
            },
            client: {
                apikey: null,
                policySettings: {
                    policy: null,
                    signature: null,
                },
                cname: null,
            },
            fieldName: ''
        };

    function Plugin(element, options) {
        this.element = element;
        this.containerElement = null;
        this.settings = $.extend(true, {}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
        this.requiredValues = {
            url: 'url',
            size: 'size',
            name: 'originalPath',
            mimetype: 'mimetype'
        };
    }

    $.extend(Plugin.prototype, {
        init: function () {
            const initial_values = this.settings.initial_values || [];

            this._initDOM();
            this._setValues(initial_values);
            this._initFilePicker();
            $(document).trigger('multiFilePickerPlugin.ready');
        },
        reinitialise: function (settings) {
            let initial_values;

            this.settings = $.extend(true, {}, defaults, settings);

            initial_values = this.settings.initial_values || [];

            this._setValues(initial_values);
            this._initFilePicker();
        },
        _initDOM: function () {
            const $outer = $('<div>').addClass('multi-file-picker'),
                $button = $('<button type="button" class="btn btn-default">').text(this.settings.text.upload_files),
                $text = $('<h3 style="margin-top: 10px"></h3>');

            this.containerElement = $outer.get(0);

            $outer.append($button.show());
            $outer.append($text.hide());

            this._getElement().append($outer);
            $(document).trigger('multiFilePickerPlugin.DOMReady');
        },
        _setValues: function (values) {
            const fieldName = this.settings.fieldName,
                container = this._getContainer();

            container.find('input[name="' + fieldName + '"]').remove();

            $.each(values, function(index, value) {
                const valueField = $('<input type="hidden" />').attr('name', fieldName).attr('value', value);
                container.append(valueField);
            });

            if (values.length > 0) {
                this._getText().text(this.settings.text.files_uploaded + ' ' + values.length).show();
                this._getButton().hide();
                return;
            }

            this._getText().text('').hide();
            this._getButton().show();
        },
        _getText: function () {
            return this._getContainer().find('h3');
        },
        _getElement: function () {
            return $(this.element);
        },
        _getContainer: function () {
            return $(this.containerElement);
        },
        _getButton: function () {
            return this._getContainer().find('button');
        },
        _initFilePicker: function initFilePicker() {
            this._getButton().click(function () {
                const client = filestackHelpers.getClient(this.settings.client.apikey,this.settings.client.policySettings, this.settings.client.cname);
                client.pick(this.settings.picker).then(this._handleFileSelection.bind(this));
                return false;
            }.bind(this));
        },
        _handleFileSelection: function (response) {
            let values = [];

            if (response['filesUploaded'] === undefined) {
                return false;
            }
            let requiredValues = this.requiredValues;

            $.each(response['filesUploaded'], function (i, f) {
                let value;

                if (f['url'] === undefined) {
                    return;
                }
                let properties = {};
                $.each(requiredValues, function (key, value) {
                    properties[key] = f[value];
                });

                values.push(JSON.stringify(properties));
            });

            this._setValues(values);
        }
    });

    $.fn[pluginName] = function (options) {
        const argsForCall = Array.prototype.slice.call(arguments, 1);
        this.each(function () {
            let existing, pluginDataKey = "plugin_" + pluginName;
            // if options is a string, assume we're trying to passively call a method on an existing instance and pass any further args to the method
            if (typeof options === 'string') {
                existing = $.data(this, pluginDataKey);
                if (existing && {}.toString.call(existing[options]) === '[object Function]') {
                    existing[options].apply(existing, argsForCall);
                }
            } else if (!$.data(this, pluginDataKey)) {
                $.data(this, pluginDataKey, new Plugin(this, options));
            }
        });

        return this;
    };
};
