import filestack from 'filestack-js';

const commonFileOpts = {
    fromSources: ['local_file_system'],
    accept: ['.doc', '.docx', '.xls', '.xlsx', '.ppt', '.pptx', '.bmp', '.gif', '.tiff', '.ai', '.psd', '.png', '.jpeg', '.jpg', '.pdf', '.mp3'],
    storeTo: {
        location: 's3'
    }
};


const getClient = (function () {
    // Minor optimisation to re-use the same client if all properties are the same as previous call, otherwise creates a new client
    let previousClient = null,
        previousClientSettings = {
            apikey: null,
            policySettings: null,
            cname: null
        };

    return function (apikey, policySettings, cname) {
        let newSettings = {apikey: apikey, policySettings: policySettings, cname: cname},
            isSameSettings = JSON.stringify(newSettings) === JSON.stringify(previousClientSettings);

        if (previousClient === null || !isSameSettings) {
            previousClient = filestack.init(apikey, policySettings, cname);
            previousClientSettings = newSettings;
        }

        return previousClient;
    };
})();

export default {getClient};
