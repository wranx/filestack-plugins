import filestackHelpers from './src/filestackHelpers';
import filePickerInit from './src/filePickerPlugin';
import multiFilePickerInit from './src/multiFilePickerPlugin';
import imagePickerInit from './src/imagePickerPlugin';
import fileViewerInit from './src/fileViewerPlugin';

(function(jQuery, undefined){
    if (jQuery === undefined) {
        throw "jQuery must be loaded ahead of filestack plugins";
    }
})(jQuery);

// Init jquery plugins
filePickerInit(filestackHelpers, jQuery, window, document);
multiFilePickerInit(filestackHelpers, jQuery, window, document);
imagePickerInit(filestackHelpers, jQuery, window, document);
fileViewerInit(filestackHelpers, jQuery, window, document);

export default filestackHelpers;
